Source: golang-github-philhofer-fwd
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Tim Potter <tpot@hpe.com>,
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any (>= 2:1.20~),
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-philhofer-fwd
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-philhofer-fwd.git
Homepage: https://github.com/philhofer/fwd
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/philhofer/fwd

Package: golang-github-philhofer-fwd-dev
Architecture: all
Depends: ${misc:Depends},
Multi-Arch: foreign
Description: Buffered Reader/Writer
 The fwd package provides a buffered reader and writer. Each has methods that
 help improve the encoding/decoding performance of some binary protocols.
 .
 The fwd.Writer and fwd.Reader type provide similar functionality to their
 counterparts in bufio, plus a few extra utility methods that simplify
 read-ahead and write-ahead. I wrote this package to improve serialization
 performance for http://github.com/philhofer/msgp, where it provided about a 2x
 speedup over bufio. However, care must be taken to understand the semantics of
 the extra methods provided by this package, as they allow the user to access
 and manipulate the buffer memory directly.
